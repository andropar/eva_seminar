package com.mycompany.control;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URL;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.mycompany.entity.Company;
import com.mycompany.testutils.DeploymentDescriptor;

@RunWith(Arquillian.class)
public class CompanyServiceArqTest {
	

	@ArquillianResource
	private URL contextPath;

	@EJB
	private CompanyService companyService;

	@Deployment(testable = true, order = 1, name = "crmDemoWebArchive", managed = true)
	public static WebArchive createDeployment() {
		return DeploymentDescriptor.createDeployment();
	}

	@Test
	@InSequence(1)
	public void testSaveCompany() {
		try {
			companyService.saveCompany(new Company("GISA GmbH"));
		} catch (ValidationException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	@InSequence(2)
	public void testFindAllCompanies() {
		assertEquals(1, companyService.listAllCompanies().size());
	}
	
	@Test
	@InSequence(3)
	public void testFindCompanyById() {		
		// ID startet bei 2 statt 1, warum?
		assertEquals("GISA GmbH", companyService.findCompanyById(2l).getName());
	}
	
	@Test
	@InSequence(4)
	public void testFindCompanies() {		
		assertEquals(1, companyService.findCompanies("GISA").size());
	}
	
	@Test
	@InSequence(5)
	public void testUpdateCompany() throws ValidationException {
		Company company = companyService.findCompanyById(2l);
		company.setName("NewName");
		companyService.updateCompany(company);
		assertEquals("NewName", companyService.findCompanyById(2l).getName());
	}
	
	@Test
	@InSequence(6)
	public void testDeleteCompany(){		
		companyService.deleteCompany(2l);
		assertEquals(0, companyService.listAllCompanies().size());
	}

}
