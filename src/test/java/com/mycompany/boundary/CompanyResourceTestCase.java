package com.mycompany.boundary;

import static org.junit.Assert.assertEquals;

import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.ejb.EJB;
import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.extension.rest.client.ArquillianResteasyResource;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.resteasy.plugins.providers.RegisterBuiltin;
import org.jboss.resteasy.spi.ResteasyProviderFactory;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.mycompany.control.CompanyService;
import com.mycompany.entity.Company;
import com.mycompany.testutils.DeploymentDescriptor;

@RunWith(Arquillian.class)
public class CompanyResourceTestCase {
	

	@Deployment(testable = true, order = 1, name = "crmDemoWebArchive", managed = true)
	public static WebArchive createDeployment() {
		return DeploymentDescriptor.createDeployment();
	}

    /**
     * The context path of the deployed application.
     */
    @ArquillianResource
    private URL contextPath;
    
    @EJB
    private CompanyService companyService;


    /**
     * Sets up the test environment.
     */
    @BeforeClass
    public static void setUpClass() {
        RegisterBuiltin.register(ResteasyProviderFactory.getInstance());
    }

    
    @Test
    @InSequence(1)
    public void testSaveCompany(@ArquillianResteasyResource("rest") ICompanyResource companyResource) throws URISyntaxException {
    	Company company = new Company("GISA GmbH");
    	Response response = companyResource.saveCompany(company);
    	assertEquals("The request didn't succeeded.", Response.Status.CREATED.getStatusCode(), response.getStatus());
    }
    
    @Test
    @InSequence(2)
    public void testFindCompanyById(@ArquillianResteasyResource("rest") ICompanyResource companyResource) throws URISyntaxException {    	
    	Response response = companyResource.findCompanyById(2L);
    	assertEquals("The request didn't succeeded.", Response.Status.OK.getStatusCode(), response.getStatus());
    }
    
    @Test
    @InSequence(3)
    public void testFindCompanies(@ArquillianResteasyResource("rest") ICompanyResource companyResource) throws URISyntaxException {    	
    	Response response = companyResource.findCompanies("GISA");
    	assertEquals("The request didn't succeeded.", Response.Status.OK.getStatusCode(), response.getStatus());
    }
    
    @Test
    @InSequence(4)
    public void testDeleteCompany(@ArquillianResteasyResource("rest") ICompanyResource companyResource) throws URISyntaxException {    	
    	Response response = companyResource.deleteCompany(2l);
    	assertEquals("The request didn't succeeded.", Response.Status.OK.getStatusCode(), response.getStatus());
    }
}
